# Use a slim Node.js image as the base
FROM node:18-alpine AS builder

# Set the working directory
WORKDIR /app

# Copy package.json and install dependencies
COPY package*.json ./
RUN yarn install --production

# Create a non-production image for serving the app
FROM node:18-alpine

# Copy package.json and lock file (if applicable)
COPY package*.json ./

# Copy the build output from the builder stage
COPY --from=builder /app/dist /app

# Set the working directory
WORKDIR /app

# Expose the port Vite uses by default (5173)
EXPOSE 5173

# Start the development server
CMD [ "yarn", "run", "dev" ]
