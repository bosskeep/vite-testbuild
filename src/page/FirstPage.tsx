import { Button } from "@mantine/core";
import { useState } from "react";

function FirstPage() {
  const [count, setCount] = useState(0);
  return (
    <>
      <h1>First Page</h1>

      <Button
        bg={"#FFBE05"}
        c={"#1458B2"}
        radius="xl"
        onClick={() => setCount((count) => count + 1)}
      >
        count is {count}
      </Button>
    </>
  );
}

export default FirstPage;
